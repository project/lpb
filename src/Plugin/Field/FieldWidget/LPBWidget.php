<?php

namespace Drupal\lpb\Plugin\Field\FieldWidget;

use Drupal\Core\Form\FormStateInterface;
use Drupal\layout_paragraphs\Plugin\Field\FieldWidget\LayoutParagraphsWidget;
use Drupal\paragraphs_browser\Plugin\Field\FieldWidget\ParagraphsBrowserWidgetTrait;

/**
 * Layout paragraphs widget.
 *
 * @FieldWidget(
 *   id = "lpb",
 *   label = @Translation("Layout Paragraphs Browser"),
 *   description = @Translation("Layout builder for paragraphs a paragraphs browser."),
 *   multiple_values = TRUE,
 *   field_types = {
 *     "entity_reference_revisions"
 *   },
 * )
 */
class LPBWidget extends LayoutParagraphsWidget {

  use ParagraphsBrowserWidgetTrait;

}
