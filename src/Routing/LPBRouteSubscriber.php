<?php

namespace Drupal\lpb\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class LPBRouteSubscriber.
 *
 * @package Drupal\lpb\Routing
 */
class LPBRouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection): void {
    // @todo this alter shouldn't be necessary, we should not hijack the route of layout_paragraphs.
    // Check if the route exists before altering it.
    if ($route = $collection->get('layout_paragraphs.builder.choose_component')) {
      $route->setDefault('_controller', '\Drupal\lpb\Controller\LPBController::list');
    }
  }

}
