<?php

namespace Drupal\lpb\Controller;

use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Render\Element;
use Drupal\paragraphs_browser\Entity\BrowserType;
use Drupal\Core\Link;
use Drupal\layout_paragraphs\Controller\ChooseComponentController;
use Drupal\layout_paragraphs\LayoutParagraphsLayout;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class LPBController.
 *
 * @package Drupal\lpb\Controller
 */
class LPBController extends ChooseComponentController implements ContainerInjectionInterface {

  /**
   * The Form Builder service.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * LPBController constructor.
   *
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The Form Builder service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info service.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher service.
   */
  public function __construct(FormBuilderInterface $form_builder, EntityTypeBundleInfoInterface $entity_type_bundle_info, EventDispatcherInterface $event_dispatcher) {
    $this->formBuilder = $form_builder;
    parent::__construct($entity_type_bundle_info, $event_dispatcher);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): LPBController {
    return new static(
      $container->get('form_builder'),
      $container->get('entity_type.bundle.info'),
      $container->get('event_dispatcher')
    );
  }

  /**
   * Override the methods you need from ChooseComponentController.
   */
  public function list(Request $request, LayoutParagraphsLayout $layout_paragraphs_layout): AjaxResponse|array {
    $context = $this->getContextFromRequest($request);
    // If inserting a new item adjacent to a sibling component, the region
    // passed in the URL will be incorrect if the existing sibling component
    // was dragged into another region. In that case, always use the existing
    // sibling's region.
    if ($context['sibling_uuid']) {
      $sibling = $layout_paragraphs_layout->getComponentByUuid($context['sibling_uuid']);
      $context['region'] = $sibling->getRegion();
    }
    $types = $this->getAllowedComponentTypes($layout_paragraphs_layout, $context);
    // If there is only one type to render,
    // return the component form instead of a list of links.
    if (count($types) === 1) {
      return $this->componentForm(key($types), $layout_paragraphs_layout, $context);
    }

    // If there is no browser selected, either it's not our widget or it's
    // misconfigured. In both cases, fall back to the layout_paragraphs-widget.
    $browser = $layout_paragraphs_layout->getSetting('paragraphs_browser', FALSE);
    if ($browser === FALSE) {
      return parent::list($request, $layout_paragraphs_layout);
    }

    // If we have no Browser, fall back to the default controller.
    $paragraphs_browser_type = BrowserType::load($browser);
    if ($paragraphs_browser_type === NULL) {
      return parent::list($request, $layout_paragraphs_layout);
    }

    return $this->paragraphsBrowser($types, $paragraphs_browser_type, $layout_paragraphs_layout, $context);
  }

  /**
   * @param array $types
   *   The component types.
   * @param \Drupal\paragraphs_browser\Entity\BrowserType $paragraphs_browser_type
   *   The selected browser type to show the paragraphs.
   * @param \Drupal\layout_paragraphs\LayoutParagraphsLayout $layout_paragraphs_layout
   *   The layout paragraphs layout object.
   * @param array $context
   *   The context for the paragraphs-browser form.
   *
   * @return array
   *   The form array to render the paragraphs-browser.
   */
  protected function paragraphsBrowser(array $types, BrowserType $paragraphs_browser_type, LayoutParagraphsLayout $layout_paragraphs_layout, array $context): array {
    // Load the paragraphs_browser-Form.
    $field_config = $layout_paragraphs_layout->getParagraphsReferenceField()
      ->getFieldDefinition();

    $form = $this->formBuilder
      ->getForm('Drupal\paragraphs_browser\Form\ParagraphsBrowserForm', $field_config, $paragraphs_browser_type, $context['parent_uuid']);

    // Go through all to the browser known types.
    $browser_group_types = $paragraphs_browser_type->map;
    foreach ($browser_group_types as $browser_group_type_id => $browser_group) {
      // Unset the ones which we don't want, because
      // getAllowedComponentTypes() didn't have them in the result.
      if (!isset($types[$browser_group_type_id])) {
        unset($form['paragraph_types'][$browser_group][$browser_group_type_id]);
        // If this was the last item in the group, there should only be
        // "label" left. In that case we can remove the group entirely.
        $paragraphTypesGroup = $form['paragraph_types'][$browser_group] ?? NULL;
        if (!is_array($paragraphTypesGroup) || count(Element::children($paragraphTypesGroup)) <= 1) {
          unset($form['paragraph_types'][$browser_group]);
        }
      }
    }

    // Implement the button-urls the way layout_paragraphs does.
    foreach ($types as $type_id => $type) {
      // Don't forget about the types which aren't in any group.
      // They'll be found in the "others" group, which has the key "_na".
      $browser_group = $browser_group_types[$type_id] ?? '_na';

      // Create the Link with the class "button",
      // so it looks just like any button.
      $link = new Link($this->t('Add'), $type['url_object']);
      $renderable_link = $link->toRenderable();
      $renderable_link['#attributes'] = ['class' => ['button', 'use-ajax']];

      // Replace the "add_more"-Button with our new "button"-link.
      $form['paragraph_types'][$browser_group][$type_id]['add_more'] = $renderable_link;
    }

    return [
      '#title' => $this->t('Choose a component'),
      'form' => $form,
    ];
  }

}
