<?php

namespace Drupal\lpb\Element;

use Drupal\Core\Url;
use Drupal\Core\Render\Markup;
use Drupal\Component\Serialization\Json;
use Drupal\layout_paragraphs\Utility\Dialog;
use Drupal\Core\Render\Element\RenderElement;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\layout_paragraphs\Element\LayoutParagraphsBuilder;

/**
 * Defines a render element for building the Layout Builder UI.
 *
 * @RenderElement("layout_paragraphs_builder")
 *
 * @internal
 *   Plugin classes are internal.
 */
class LpbLayoutParagraphsBuilder extends LayoutParagraphsBuilder implements ContainerFactoryPluginInterface {

  /**
   * Returns the render array for a insert component button.
   * 
   * This method overrides the original element insert component
   * to allow a wider dialog. See issues
   * https://www.drupal.org/project/layout_paragraphs/issues/3341449
   * and
   * https://www.drupal.org/project/lpb/issues/3398672
   *
   * @param array[] $route_params
   *   The route parameters for the link.
   * @param array[] $query_params
   *   The query paramaters for the link.
   * @param int $weight
   *   The weight of the button element.
   * @param array[] $classes
   *   A list of classes to append to the container.
   *
   * @return array
   *   The render array.
   */
  protected function insertComponentButton(array $route_params = [], array $query_params = [], int $weight = 0, array $classes = []) {
    return [
      '#theme' => 'layout_paragraphs_insert_component_btn',
      '#title' => Markup::create('<span class="visually-hidden">' . $this->t('Choose component') . '</span>'),
      '#weight' => $weight,
      '#attributes' => [
        'class' => array_merge(['lpb-btn--add', 'use-ajax'], $classes),
        'data-dialog-type' => 'dialog',
        'data-dialog-options' => Json::encode(Dialog::dialogSettings($this->layoutParagraphsLayout)),
      ],
      '#url' => Url::fromRoute('layout_paragraphs.builder.choose_component', $route_params, ['query' => $query_params]),
    ];
  }

}
